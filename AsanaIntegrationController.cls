global class AsanaIntegrationController {
	
    @future(callout=true)
    public static void createTask(Id meetingId){
        List<Meeting__c> meetingLst = [SELECT Id, Due_Date__c, Responsible_Person__c, Responsible_Person__r.Email, Account__c, Account__r.Name, Event__c, Event__r.Name, Description__c, Asana_GID__c FROM Meeting__c WHERE Id =: meetingId];
        Meeting__c meet = meetingLst[0];
        
        Asana_Integration__c asanaInt = Asana_Integration__c.getOrgDefaults();
        String taskUrl = asanaInt.Create_Task_Url__c;
        String setCustomFieldsUrl = asanaInt.Set_Task_Custom_Fields_Url__c;
        String accessToken = asanaInt.Access_Token__c;
        String asanaMettingProjectId = asanaInt.Asana_Meeting_Project_Id__c;
        String envURL = asanaInt.Environment_URL__c;
        Date dueDate = meet.Due_Date__c;
        String dueOn = Datetime.newInstance(dueDate.year(), dueDate.month(), dueDate.day()).format('yyyy-MM-dd');
        String nameOfTask = meet.Event__r.Name + ' - ' + meet.Account__r.Name;
        
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(taskUrl);
        request.setHeader('Authorization', 'Bearer '+accessToken);
        request.setBody('notes='+EncodingUtil.urlEncode(meet.Description__c+'\n'+envUrl+'/'+meet.Account__c, 'UTF-8')+'&name='+EncodingUtil.urlEncode(nameOfTask, 'UTF-8')+'&projects='+asanaMettingProjectId+'&assignee='+meet.Responsible_Person__r.Email+'&due_on='+dueOn);
		request.setMethod('POST');
		HttpResponse response = http.send(request);
        if (response.getStatusCode() == 201) {
            TaskData obj = (TaskData) JSON.deserialize(response.getBody(), TaskData.class);
            meet.Asana_GID__c = obj.data.gid;
            update meet;
        }
    }
    
    class TaskData{
        public Data data {get; set;}
    }
    
    class Data{
        public String gid {get; set;}
        public String id {get; set;}
    }
    
}