@isTest
global class AsanaIntegrationMock implements HttpCalloutMock {
	global HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"data": {"gid": "12345678", "id": "876543321"}}');
        res.setStatusCode(201);
        return res;
	}
}