global class AsanaIntegrationTaskBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	public String query = 'SELECT Id, Asana_GID__c, Aufwand__c, Description__c, Meeting_Name__c, Name, Next_Step__c, Priorit_t__c, Veranstaltung__c, Veranstaltung_1__c, Veranstaltung_2__c, Veranstaltung_3__c, Year__c FROM Meeting__c WHERE Asana_GID__c!=null AND Asana_In_Sync__c=false';
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<Meeting__c> records) {
		for(Meeting__c meet:records){
			try {
                Asana_Integration__c asanaInt = Asana_Integration__c.getOrgDefaults();
                String veranstaltungFieldId = asanaInt.Veranstaltung_Field_Id__c;
                String aufwandFieldId = asanaInt.Aufwand_Field_Id__c;
                String priorittFieldId = asanaInt.Priorit_t_Field_Id__c;
                String veranstaltung1FieldId = asanaInt.Veranstaltung_1_Field_Id__c;
                String veranstaltung2FieldId = asanaInt.Veranstaltung_2_Field_Id__c;
                String veranstaltung3FieldId = asanaInt.Veranstaltung_3_Field_Id__c;
                
                List<String> customFieldList = new List<String>();
                if(meet.Veranstaltung__c != null){
                    customFieldList.add('\"'+veranstaltungFieldId+'\":\"'+meet.Veranstaltung__c+'\"');
                }
                if(meet.Aufwand__c != null){
                    customFieldList.add('\"'+aufwandFieldId+'\":\"'+meet.Aufwand__c+'\"');
                }
                if(meet.Priorit_t__c != null){
                    customFieldList.add('\"'+priorittFieldId+'\":\"'+meet.Priorit_t__c+'\"');
                }
                if(meet.Veranstaltung_1__c != null){
                    customFieldList.add('\"'+veranstaltung1FieldId+'\":\"'+meet.Veranstaltung_1__c+'\"');
                }
                if(meet.Veranstaltung_2__c != null){
                    customFieldList.add('\"'+veranstaltung2FieldId+'\":\"'+meet.Veranstaltung_2__c+'\"');
                }
                if(meet.Veranstaltung_3__c != null){
                    customFieldList.add('\"'+veranstaltung3FieldId+'\":\"'+meet.Veranstaltung_3__c+'\"');
                }
                String customFieldData = '{"data": {\"custom_fields\":{'+ String.join(customFieldList, ',') +'}}}';
                System.debug('customFieldData='+customFieldData);
                
                String setCustomFieldsUrl = asanaInt.Set_Task_Custom_Fields_Url__c;
                String accessToken = asanaInt.Access_Token__c;
                String asanaMettingProjectId = asanaInt.Asana_Meeting_Project_Id__c;
                
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndpoint(setCustomFieldsUrl+meet.Asana_GID__c);
                request.setHeader('Authorization', 'Bearer '+accessToken);
                request.setHeader('Content-Type', 'application/json');
                request.setBody(customFieldData);
                request.setMethod('PUT');
                HttpResponse response = http.send(request);
                if (response.getStatusCode() == 201) {
                    meet.Asana_In_Sync__c = true;
                }
			}
			catch (Exception e) {         
				System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber() );           
			}
		}
        update records;
	}
	
	global void finish(Database.BatchableContext BC){}
    
}