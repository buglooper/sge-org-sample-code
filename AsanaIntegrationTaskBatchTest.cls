@isTest
public class AsanaIntegrationTaskBatchTest {
	public static testMethod void method1(){
        Asana_Integration__c asanaInt = Asana_Integration__c.getOrgDefaults();
        asanaInt.Veranstaltung_Field_Id__c='Test';
        asanaInt.Aufwand_Field_Id__c='Test';
        asanaInt.Priorit_t_Field_Id__c='Test';
        asanaInt.Veranstaltung_1_Field_Id__c='Test';
        asanaInt.Veranstaltung_2_Field_Id__c='Test';
        asanaInt.Veranstaltung_3_Field_Id__c='Test';
        asanaInt.Create_Task_Url__c='http://www.google.com';
        asanaInt.Set_Task_Custom_Fields_Url__c='http://www.google.com';
        asanaInt.Access_Token__c='Test';
        asanaInt.Asana_Meeting_Project_Id__c='Test';
        insert asanaInt;
        
        String veranstaltungVal = '';
        String aufwandVal = '';
        String priorittVal = '';
        String veranstaltung1Val = '';
        String veranstaltung2Val = '';
        String veranstaltung3Val = '';
        Schema.DescribeFieldResult fieldResult = Meeting__c.Aufwand__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			aufwandVal = f.getValue();
            break;
        }
        fieldResult = Meeting__c.Veranstaltung__c.getDescribe();
        ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			veranstaltungVal = f.getValue();
            break;
        }
        fieldResult = Meeting__c.Priorit_t__c.getDescribe();
        ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			priorittVal = f.getValue();
            break;
        }
        fieldResult = Meeting__c.Veranstaltung_1__c.getDescribe();
        ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			veranstaltung1Val = f.getValue();
            break;
        }
        fieldResult = Meeting__c.Veranstaltung_2__c.getDescribe();
        ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			veranstaltung2Val = f.getValue();
            break;
        }
        fieldResult = Meeting__c.Veranstaltung_3__c.getDescribe();
        ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple){
			veranstaltung3Val = f.getValue();
            break;
        }
		
        Account account = TestDataFactory.account;
		Pricebook2 pricebookAussteller =  TestDataFactory.pricebookAussteller;
		Pricebook2 pricebookMarketing = TestDataFactory.pricebookMarketing;
		Product2 productAussteller = TestDataFactory.productAussteller;
		Product2 productMarketing = TestDataFactory.productMarketing;
        insert new List<SObject> {account, pricebookAussteller, pricebookMarketing, productMarketing, productAussteller};
		Id standardPBId = Test.getStandardPricebookId();
		PricebookEntry standardPriceAussteller = new PricebookEntry(Pricebook2Id = standardPBId, 
		                                                             Product2Id = productAussteller.Id, 
		                                                             UnitPrice = 2500,
		                                                             IsActive = true
		                                                             );
		PricebookEntry standardPriceMarketing = new PricebookEntry(Pricebook2Id = standardPBId, 
		                                                             Product2Id = productMarketing.Id, 
		                                                             UnitPrice = 1500,
		                                                             IsActive = true
		                                                             );
		insert new List<SObject> {standardPriceMarketing, standardPriceAussteller};
		PricebookEntry pricebookEntryAussteller = TestDataFactory.pricebookEntryAussteller;
		PricebookEntry pricebookEntryMarketing = TestDataFactory.pricebookEntryMarketing;
		insert new List<SObject> {pricebookEntryAussteller, pricebookEntryMarketing};
		Event__c event = TestDataFactory.event;
		insert event;
		
        Meeting__c meeting = new Meeting__c();
        meeting.Account__c = account.Id;
        meeting.Aufwand__c = aufwandVal;
        meeting.Description__c = 'Test Description';
        meeting.Event__c = event.Id;
        meeting.Priorit_t__c = priorittVal;
        meeting.Veranstaltung__c = veranstaltungVal;
        meeting.Veranstaltung_1__c = veranstaltung1Val;
        meeting.Veranstaltung_2__c = veranstaltung2Val;
        meeting.Veranstaltung_3__c = veranstaltung3Val;
        meeting.Asana_GID__c = '12345';
        insert meeting;
        
        Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new AsanaIntegrationMock());
        	AsanaIntegrationTaskSchedule asanaSch = new AsanaIntegrationTaskSchedule();      
			String sch = '0 0 23 * * ?';
			System.schedule('Test check', sch, asanaSch);
        Test.stopTest();
    }
}