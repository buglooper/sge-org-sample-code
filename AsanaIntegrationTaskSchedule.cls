global class AsanaIntegrationTaskSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
        Asana_Integration__c asanaInt = Asana_Integration__c.getOrgDefaults();
		AsanaIntegrationTaskBatch b = new AsanaIntegrationTaskBatch(); 
		Database.executebatch(b, Integer.valueOf(asanaInt.Batch_Size__c));
	}
}